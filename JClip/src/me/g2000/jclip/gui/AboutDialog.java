package me.g2000.jclip.gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class AboutDialog extends JFrame implements WindowListener {
	private static final long serialVersionUID = -1793403746545065976L;
	/**
	 * @author g2000
	 */
	
	private JLabel about_text;
	private JLabel about_logo;
	private GridBagConstraints gbc;

	public AboutDialog() {
//		set title
		super("About");
//		set close operation --> do nothing
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
//		set layout and window styles
		setLayout(new GridBagLayout());
		setResizable(false);
//		initialize form components
		initComponents();
//		add a window listener to control form closing
		addWindowListener(this);
	}

	private void initComponents() {
//		setup dialog components
		gbc = new GridBagConstraints();

		about_logo = new JLabel(new ImageIcon("res/pics/logo.png"));
		about_text = new JLabel("JClip v0.1 by g2000");

		gbc.gridx = 0;
		gbc.gridy = 0;
		add(about_logo, gbc);
		gbc.gridx = 0;
		gbc.gridy = 1;
		add(about_text, gbc);
		pack();
	}

	@Override
	public void windowClosing(WindowEvent e) {
//		hide frame
		setVisible(false);
	}
//	not used
	@Override
	public void windowActivated(WindowEvent e) {}
	@Override
	public void windowClosed(WindowEvent e) {}
	@Override
	public void windowDeactivated(WindowEvent e) {}
	@Override
	public void windowDeiconified(WindowEvent e) {}
	@Override
	public void windowIconified(WindowEvent e) {}
	@Override
	public void windowOpened(WindowEvent e) {}
}
