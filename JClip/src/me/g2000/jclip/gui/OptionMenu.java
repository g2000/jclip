package me.g2000.jclip.gui;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JSpinner;

import me.g2000.jclip.misc.Configuration;
import me.g2000.jclip.misc.ErrorHandler;

public class OptionMenu extends JFrame implements WindowListener, ActionListener {
	private static final long serialVersionUID = 5922385839669789237L;
	/**
	 * @author g2000
	 */

	private GridBagConstraints gbc;
	private JCheckBox chb_enabled;
	private JCheckBox chb_hideCopiedText;
	private JCheckBox chb_restoreHistory;
	private JLabel lab_history;
	private JSpinner spn_history;
	private JButton btn_save;
	private JButton btn_cancel;
	
	private Configuration config;

	public OptionMenu() {
//		set title, configure frame and initialize form components
		super("Options..");
		setResizable(false);
		setLayout(new GridBagLayout());
		initComponents();
//		pack the frame
		pack();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
//		SAVE EVENT
		if (e.getSource() == btn_save) {
//			update all config keys
			config.setKey("historySize", spn_history.getValue().toString());
			config.setKey("enabled", String.valueOf(chb_enabled.isSelected()));
			config.setKey("restoreHistory", String.valueOf(chb_restoreHistory.isSelected()));
			config.setKey("hideCopiedText", String.valueOf(chb_hideCopiedText.isSelected()));
//			try to save
			try {
				config.save();
			} catch (IOException ex) {
				ErrorHandler.showError("Could not save configuration.");
			}
			setVisible(false);
		}
//		CANCEL EVENT
		if (e.getSource() == btn_cancel) {
			setVisible(false);
		}
	}

	private void initComponents() {
//		try to load the config file
		try {
			config = new Configuration("res/conf/configuration.cfg");
		} catch (IOException e) {
			ErrorHandler.die(ErrorHandler.CORRUPT_FILES);
		}
//		<---- START SETUP_FRAME
		gbc = new GridBagConstraints();
		
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.fill = 1;
		lab_history = new JLabel("History elements:");
		add(lab_history, gbc);
		
		gbc.gridx = 1;
		gbc.gridy = 0;
		gbc.fill = 0;
		spn_history = new JSpinner();
		spn_history.setPreferredSize(new Dimension(50, 20));
		add(spn_history, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 1;
		gbc.fill = 1;
		chb_enabled = new JCheckBox("Enabled");
		add(chb_enabled, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 2;
		chb_hideCopiedText = new JCheckBox("Hide history text");
		add(chb_hideCopiedText, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 3;
		chb_restoreHistory = new JCheckBox("Restore history on restart");
		add(chb_restoreHistory, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 4;
		gbc.fill = 1;
		btn_save = new JButton("Save");
		btn_save.setPreferredSize(new Dimension(100, 25));
		btn_save.addActionListener(this);
		add(btn_save, gbc);

		gbc.gridx = 1;
		gbc.gridy = 4;
		btn_cancel = new JButton("Cancel");
		btn_cancel.setPreferredSize(new Dimension(100, 25));
		btn_cancel.addActionListener(this);
		add(btn_cancel, gbc);
//		                    END SETUP_FRAME ---->
	}

	public void showFrame() {
//		update the component values and set the frame visible
		spn_history.setValue(Integer.parseInt(config.getKey("historySize")));
		chb_enabled.setSelected(Boolean.parseBoolean(config.getKey("enabled")));
		chb_hideCopiedText.setSelected(Boolean.parseBoolean(config.getKey("hideCopiedText")));
		chb_restoreHistory.setSelected(Boolean.parseBoolean(config.getKey("restoreHistory")));
		setVisible(true);
	}
	
//	hide the frame
	@Override
	public void windowClosing(WindowEvent e) {
		setVisible(false);
	}
//	not used
	@Override
	public void windowActivated(WindowEvent e) {}
	@Override
	public void windowClosed(WindowEvent e) {}
	@Override
	public void windowDeactivated(WindowEvent e) {}
	@Override
	public void windowDeiconified(WindowEvent e) {}
	@Override
	public void windowIconified(WindowEvent e) {}
	@Override
	public void windowOpened(WindowEvent e) {}
}
