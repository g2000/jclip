package me.g2000.jclip.misc;

public class NotSupportedException extends Exception {
	private static final long serialVersionUID = 3048615502941251415L;

	/**
	 * @author g2000
	 */

	// pass the message to the super class
	public NotSupportedException(String message) {
		super(message);
	}

}
