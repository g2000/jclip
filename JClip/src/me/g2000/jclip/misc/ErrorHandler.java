package me.g2000.jclip.misc;

import javax.swing.JOptionPane;

public abstract class ErrorHandler {
	
	/**
	 * @author g2000
	 */
	
	public static final int CORRUPT_FILES = 0;
	public static final int LOOK_AND_FEEL_ERROR = 1;
	public static final int TRAY_NOT_SUPPORTED = 2;
	public static final int TRAY_ICON_IO_EXCEPTION = 3;

	public static void die(int reason) {
		if (reason == CORRUPT_FILES)
			die("Invalid program files!");
		if (reason == LOOK_AND_FEEL_ERROR)
			die("Your OS does not support the Java LookAndFeel.");
		if (reason == TRAY_NOT_SUPPORTED)
			die("Your OS does not support the Java SystemTray.");
		if (reason == TRAY_ICON_IO_EXCEPTION)
			die("Could not load tray icon image");
	}
	
	public static void showError(int reason) {
		if (reason == CORRUPT_FILES)
			showError("Invalid program files!");
		if (reason == LOOK_AND_FEEL_ERROR)
			showError("Your OS does not support the Java LookAndFeel.");
		if (reason == TRAY_NOT_SUPPORTED)
			showError("Your OS does not support the Java SystemTray.");
		if (reason == TRAY_ICON_IO_EXCEPTION)
			showError("Could not load tray icon image");
	}

	public static void die(String message) {
		JOptionPane.showMessageDialog(null, message);
		System.exit(-1);
	}

	public static void showError(String message) {
		JOptionPane.showMessageDialog(null, message);
	}
}
