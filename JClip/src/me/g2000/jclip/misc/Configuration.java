package me.g2000.jclip.misc;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map.Entry;
import java.util.TreeMap;

public class Configuration {

	private Path path;
	private List<String> lines;
	private TreeMap<String, String> filemap;

	public Configuration(String uri) throws IOException {
//		initialize path and filemap
		this.path = Paths.get(uri);
		this.filemap = new TreeMap<String, String>();
//		parse the file
		readFile();
	}

	private void readFile() throws IOException {
		String[] splitResult;
		lines = Files.readAllLines(path);
//		loop for each line
		for (int i = 0; i < lines.size(); i++) {
//			sort out some lines such as comments and blank lines
			if (!lines.get(i).startsWith("#") && !lines.get(i).equals("")
					&& lines.get(i).contains("=")) {
				splitResult = lines.get(i).split("=");
				filemap.put(splitResult[0], splitResult[1]);
			}
		}
	}

	public void save() throws IOException {
//		clear the List<String> lines
		lines.clear();
//		add all lines from filemap to lines
		for (Entry<String, String> e : filemap.entrySet()) {
			lines.add(e.getKey() + "=" + e.getValue());
		}
//		write the lines to the configuration file
		Files.write(path, lines, StandardCharsets.UTF_8);
	}
	
	public String getKey(String key) {
		return filemap.get(key);
	}

	public void setKey(String key, String value) {
		filemap.put(key, value);
	}
}
