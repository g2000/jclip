package me.g2000.jclip.main;

import java.awt.AWTException;
import java.awt.Image;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import me.g2000.jclip.gui.AboutDialog;
import me.g2000.jclip.gui.OptionMenu;
import me.g2000.jclip.misc.ErrorHandler;
import me.g2000.jclip.misc.NotSupportedException;

public class TrayManager implements ActionListener {

	/**
	 * @author g2000
	 */

	private OptionMenu options;
	private AboutDialog about;
	
	private PopupMenu popup;
	private TrayIcon trayicon;
	private MenuItem men_about;
	private MenuItem men_exit;
	private MenuItem men_options;
	private List<MenuItem> men_history;
	private SystemTray tray;

	public TrayManager() throws NotSupportedException {
//		check if the current OS supports a system tray
		if (!SystemTray.isSupported()) {
			throw new NotSupportedException("OS does not support system tray.");
		} else {
//			initialize components and add tray icon to system tray
			initComponents();
			addToTray();
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals("About")) {
			about.setVisible(true);
		}
		if (e.getActionCommand().equals("Options")) {
			options.showFrame();
		}
		if (e.getActionCommand().equals("Exit")) {
			System.exit(0);
		}
	}

	public void addToHistory(String element) {
//		remove all history menu items from popup
		if (men_history.size() != 0) {
			for (int i = 0; i <= men_history.size() - 1; i++) {
				popup.remove(men_history.get(i));
			}
		}
//		add element to ArrayList
		men_history.add(new MenuItem(element));
//		restore all history menu items
		for (int i = 0; i <= men_history.size() - 1; i++) {
			popup.insert(men_history.get(i), 0);
		}
	}

	private void addToTray() {
//		try to add the tray icon to system tray
		try {
			tray.add(trayicon);
		} catch (AWTException ex) {
			ex.printStackTrace();
		}
	}

	private void initComponents() {
//		get the system tray and instantiate tray icon
		tray = SystemTray.getSystemTray();
		try {
			BufferedImage trayiconimage = ImageIO.read(new File("res/pics/icon.png"));
			int trayiconwidth = new TrayIcon(trayiconimage).getSize().width;
			trayicon = new TrayIcon(trayiconimage.getScaledInstance(trayiconwidth, -1, Image.SCALE_SMOOTH));
		} catch (IOException e) {
			ErrorHandler.die(ErrorHandler.TRAY_ICON_IO_EXCEPTION);
		}
//		init menus and dialogs
		popup = new PopupMenu();
		about = new AboutDialog();
		options = new OptionMenu();
//		init menu items
		men_about = new MenuItem("About");
		men_exit = new MenuItem("Exit");
		men_options = new MenuItem("Options");
		men_history = new ArrayList<MenuItem>();
//		build up popup menu
		popup.addSeparator();
		popup.add(men_options);
		popup.add(men_about);
		popup.addSeparator();
		popup.add(men_exit);
//		set the tray icon's popup menu and add ->this<- class as an action listener
		trayicon.setPopupMenu(popup);
		popup.addActionListener(this);
	}
}