package me.g2000.jclip.main;

import javax.swing.UIManager;

import me.g2000.jclip.misc.ErrorHandler;
import me.g2000.jclip.misc.NotSupportedException;

public class ClipManager {
	
	/**
	 * @author g2000
	 * @version 0.1 alpha
	 */

	private TrayManager traymgr;

	public static void main(String[] args) {
//		try to set the LookAndFeel to a system specific one
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			ErrorHandler.showError(ErrorHandler.LOOK_AND_FEEL_ERROR);
		}
//		 call class constructor
		new ClipManager();
	}
	
	public ClipManager() {
//		 initialize the main components
		initComponents();
	}

	private void initComponents() {
//		 try to initialize TrayManager, if system tray not supported: exit program
		try {
			traymgr = new TrayManager();
		} catch (NotSupportedException ex) {
			ErrorHandler.die(ErrorHandler.TRAY_NOT_SUPPORTED);
		}
	}
	

}
